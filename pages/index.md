# Programming Training Programme

## Contents
1. [<b> Introduction to Programming Logic </b>](/pages/intro_to_programming_logic.md)
2. [<b> User Interface and User Experience </b>](/pages/ui_ux.md)
3. [<b> Documentation </b>](/pages/documentating.md)
4. [<b> Markdown Coding </b>](/pages/markdown_coding.md)
5. [<b> VBA Programming </b>](/pages/vba_programming.md)
6. [<b> SQL Programming </b>](/pages/sql_programming.md)

## Todos
- [ ] Add resources for FormSG lesson
- [ ] Integrate Repository into Gitlab/ Github Pages with automation for website version
- [ ] Convert lesson notes to slideshow form for lecture

