# Introduction to Programming Logic
This chapter emphasises the most basic foundation to programming, logical thinking. This would help one understand how computers understand and execute code. <br>

There will be plentiful of basic mathematical and logical thinking references, dont give up if its difficult to comprehend from the get go.

<hr>

## Contents
1. [<b>Computational Thinking </b>](#computational-thinking)
    1. [Decomposition](#decomposition)
    2. [Pattern Recognition](#pattern-recognition)
    3. [Pattern Generalisation](#pattern-generalisation)
    4. [Algorithm Design](#algorithm-design)
2. [<b>Control Flow </b>](#control-flow)
    1. [Sequential](#sequential)
    2. [Selection](#selection)
    3. [Repetition/ Iteration](#repeition-iteration)
3. [<b>Flow Planning </b>](#flow-planning)
    1. [Flowcharts](#flowcharts)
    2. [Pseudocode](#pseudocode)
4. [<b>Variables & Data Structures </b>](#variables--data-structures)
5. [<b>Mathematical Operations (Arithmetic) </b>](#mathematical-operations-arithmetic)
6. [<b>Boolean Algebra </b>](#boolean-algebra)
    1. [Logical Operators](#logical-operators)
    2. [Truth Tables](#truth-tables)
7. [<b>Computational Thinking Cont. </b>](#computational-thinking-cont)
    1. [Functions](#functions)

<hr>

## Computational Thinking
4 key elements of Computational Thinking
- [<b>Decomposition</b>: ](#decomposition)the ability to break down a problem into sub-problems.
- [<b>Pattern recognition</b>](#pattern-recognition): the ability to notice similarities, differences, properties, or trends in data.
- [<b>Pattern generalisation</b>](#pattern-generalisation): the ability to extract unnecessary details and generalise those that are necessary in order to define a concept or idea in general terms.
- [<b>Algorithm design</b>](#algorithm-design): the ability to build a repeatable, step-by-step process to solve a particular problem.

\* Pattern generalisation may also be termed abstraction.

### Decomposition
<img src="./resources/computational-thinking_decomposition_img1.svg" height="150px"> <br>

-

### Pattern Recognition
-

### Pattern Generalisation
-

### Algorithm Design
- 


## Control Flow
... code is run from first line to last line ...

3 categories of control statements are:
1. [Sequential](#sequential)
2. [Selection](#selection)
3. [Repetition/ Iteration](#repetition-iteration)

### Sequential
<img src="./resources/control-flow_sequential_img1.svg" height="150px"> <br>

- 

### Selection
<img src="./resources/control-flow_selection_img1.svg" height="150px"> <br>

-

### Repetition/ Iteration
<img src="./resources/control-flow_repetition_img1.svg" height="150px"> <br>

-

## Flow Planning
-

### Flowcharts
Common Symbols for flowcharts:

<table>
  <th>Shape Type</th>
  <th>Shape</th>
  <th>Purpose</th>
  <th>Example Purpose</th>

<!-- Oval -->
<tr>
  <td>Oval</td>
  <td> 

  ``` mermaid
  flowchart
      id1(["Terminator"])
  ```

  </td>
  <td>Termination</td>
  <td>Start/ End</td>
</tr>

<!-- Rectangle -->
<tr>
  <td>Rectangle</td>
  <td> 

  ``` mermaid 
  flowchart LR 
      id1["Process"]
  ```

  </td>
  <td>Process</td>
  <td></td>
</tr>

<!-- Diamond -->
<tr>
  <td>Diamond</td>
  <td> 

  ``` mermaid 
  flowchart LR 
      id1{"Decision"}
  ```

  </td>
  <td>Decision</td>
  <td>If (condition), do something, else do something else</td>
</tr>

<!-- Arrow -->
<tr>
  <td>Arrow</td>
  <td> 

  ``` mermaid
  flowchart LR
      id1["Process 1"] --> id2["Process 2"]
  ```

  </td>
  <td>Flow of process</td>
  <td>Start/ End</td>
</tr>

</table>

### Pseudocode
Pseudocode is an artificial and informal language that helps programmers develop algorithms. Pseudocode is a "text-based" detail (algorithmic) design tool. The rules of Pseudocode are reasonably straightforward. All statements showing "dependency" are to be indented. These include while, do, for, if, switch. - [reference](https://www.unf.edu/~broggio/cop2221/2221pseu.htm#:~:text=Pseudocode%20is%20an%20artificial%20and,%2C%20for%2C%20if%2C%20switch.)

Combining concepts from control flow, using pseudocode for flow planning would allow one to clearly and succiently represent a task in an algorithmic manner.

### Implementation Example
While flowcharts give a visual representation and pseudocode a textual one; they both can effectively present a task step by step.

Here, I show the same process in both flowchart and pseudcode forms. <br>
The intended process is to print "hello world" 10 times.

#### Flowchart Demo
``` mermaid 
flowchart TD
  start_id(["start"]) --> id1["initialize counter to 0"]
  id1 --> id2{"counter < 10?"}
  id2 --> |no| end_id(["end"])
  id2 --> |yes| id3["print 'hello world'"]
  id3 --> id4["add 1 to counter"]
  id4 --> id2
```
#### Pseudocode Demo
```
counter = 0
while counter < 10
  print "hello world"
```

<br>
<hr>
<br>
Both flowcharts and pseudocode codes tend to look very similar to your program; of course! it is a literal broken down and simplified representation of your algorithm, only without the syntax and semantics that your respective programming language require.
<br>
<br>
Another advantage of being able to translate an algorithm into flowcharts AND pseudocode is the flexiblity; there are 2 types common programming means: textual and graphical (GUI). such examples are:

<br>

Textual:
1. VBA
2. Java

Graphical
1. Scratch
2. UI Path

<br>

## Variables & Data Structures

## Mathematical Operations (Arithmetic)

## Boolean Algebra
In mathematics and mathematical logic, Boolean algebra is the branch of algebra in which the values of the variables are the truth values true and false, usually denoted 1 and 0, respectively.

### Logical Operators
|Logical Operator|Pseudocode|VBA/ SQL/ Python|Java/ C|
|:-:|:-:|:-:|:-:|
|AND|AND|and|&&|
|OR|OR|or|\|\||
|NOT|NOT|not|!|

### Truth Tables
A common way to visually represent logical relationships by identifying all possible inputs and their respective output values - put into a table

#### Logical AND (&&)
|X|Y|X AND Y/ X && Y|
|:-:|:-:|:-:|
|0|0|0|
|1|0|0|
|0|1|0|
|1|1|1|

#### Logical OR (||)
|X|Y|X OR Y/ X \|\| Y|
|:-:|:-:|:-:|
|0|0|0|
|1|0|1|
|0|1|1|
|1|1|1|

#### Logical NOT (!)
|X| NOT X (!X)|
|:-:|:-:|
|0|1|
|1|0|

#### [Extra] !(A AND !B)
|A|B|C|!(A AND !B) OR C|
|:-:|:-:|:-:|:-:|
|0|0|0|1|
|0|0|1|1|
|0|1|0|1|
|0|1|1|1|
|1|0|0|0|
|1|0|1|1|
|1|1|0|1|
|1|1|1|1|

Still confused? Use this [truth table generator](https://web.stanford.edu/class/cs103/tools/truth-table-tool/) to automatically generate a truth table for your equation

### [Advanced] De Morgan's Laws (Summarised)
1. The complement of the union of two sets is the same as the intersection of their complements
2. The complement of the intersection of two sets is the same as the union of their complements

Simply put,
1. NOT (A AND B) = (NOT A) OR (NOT B)
2. NOT (A OR B) = (NOT A) AND (NOT B)

De Morgan's Laws are usually used to simplify complex boolean algebraic expressions.

## Computational Thinking Cont.

### Functions
